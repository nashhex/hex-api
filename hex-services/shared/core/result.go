package core

// Result indicates the result of a game
type Result string

const (
	// ResultWon indicates a won game
	ResultWon Result = "won"
	// ResultLost indicates a lost game
	ResultLost Result = "lost"
	// ResultPending indicates a game is still being played
	ResultPending Result = "pending"
)

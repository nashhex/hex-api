package core

var (
	// PlayerPrefix DynamoDB key prefix to reference a player
	PlayerPrefix = "PLY"
)

// Player contains information of a player
type Player struct {
	PlayerID    string   `json:"player_id,omitempty"`
	StudentsIDs []string `json:"students_ids,omitempty"`
}

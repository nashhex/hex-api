package core

// GameResult contains information about a game result
type GameResult struct {
	PlayerID string `json:"player_id"`
	GameID   string `json:"game_id"`
	Result   Result `json:"result"`
}

package core

// Status indicates the game status
type Status string

const (
	// StatusOngoing indicates a pending game
	StatusOngoing Status = "ongoing"
	// StatusFinished indicates a finished game
	StatusFinished Status = "finished"
)

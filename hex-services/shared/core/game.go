package core

var (
	// GamePrefix DynamoDB key prefix to reference a game
	GamePrefix = "GME"
)

// Game contains information of a game
type Game struct {
	GameID       string       `json:"game_id,omitempty"`
	PlayersIDs   []string     `json:"players_ids,omitempty"`
	LastPlayerID string       `json:"last_player_id,omitempty"`
	Status       Status       `json:"status,omitempty"`
	Board        [][][]string `json:"board,omitempty"`
}

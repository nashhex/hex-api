package storage

import (
	"errors"
	"hex-api/hex-services/shared/core"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var (
	// ErrResultNotFound is error when no player is found
	ErrResultNotFound = errors.New("player not found")

	resultTableName = os.Getenv("RESULT_TABLE_NAME")
)

// PutGameResult stores a player
func PutGameResult(result *core.GameResult) error {
	item, err := dynamodbattribute.MarshalMap(result)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(resultTableName),
	}
	_, err = dynamoClient.PutItem(input)
	return err
}

// GetGameResult obtains a game result from dynamodb
func GetGameResult(playerID, gameID string) (*core.Player, error) {
	key := map[string]*dynamodb.AttributeValue{
		"player_id": {S: aws.String(playerID)},
		"game_id":   {S: aws.String(gameID)},
	}

	input := &dynamodb.GetItemInput{
		Key:       key,
		TableName: aws.String(resultTableName),
	}

	output, err := dynamoClient.GetItem(input)
	if err != nil {
		return nil, err
	}

	if len(output.Item) == 0 {
		return nil, ErrPlayerNotFound
	}

	player := &core.Player{}
	err = dynamodbattribute.UnmarshalMap(output.Item, player)

	return player, err
}

// GetPlayerGames obtains a all the games of a player
func GetPlayerGames(playerID string) ([]*core.GameResult, error) {
	key := map[string]*dynamodb.AttributeValue{
		":id": {S: aws.String(playerID)},
	}

	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: key,
		KeyConditionExpression:    aws.String("player_id=:id"),
		TableName:                 aws.String(resultTableName),
	}

	output, err := dynamoClient.Query(input)
	if err != nil {
		return nil, err
	}

	games := []*core.GameResult{}
	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, games)

	return games, err
}

package storage

import (
	"errors"
	"hex-api/hex-services/shared/core"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var (
	// ErrPlayerNotFound is error when no player is found
	ErrPlayerNotFound = errors.New("player not found")

	playerTableName = os.Getenv("PLAYER_TABLE_NAME")
)

// PutPlayer stores a player
func PutPlayer(player *core.Player) error {
	item, err := dynamodbattribute.MarshalMap(player)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(playerTableName),
	}
	_, err = dynamoClient.PutItem(input)
	return err
}

// GetPlayer obtains a player from dynamodb
func GetPlayer(playerID string) (*core.Player, error) {
	key := map[string]*dynamodb.AttributeValue{
		"player_id": {S: aws.String(playerID)},
	}

	input := &dynamodb.GetItemInput{
		Key:       key,
		TableName: aws.String(playerTableName),
	}

	output, err := dynamoClient.GetItem(input)
	if err != nil {
		return nil, err
	}

	if len(output.Item) == 0 {
		return nil, ErrPlayerNotFound
	}

	player := &core.Player{}
	err = dynamodbattribute.UnmarshalMap(output.Item, player)

	return player, err
}

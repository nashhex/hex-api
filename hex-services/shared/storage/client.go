package storage

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-xray-sdk-go/xray"
)

var (
	dynamoClient dynamodbiface.DynamoDBAPI
)

func init() {
	client := dynamodb.New(session.Must(session.NewSession()))
	xray.AWS(client.Client)

	dynamoClient = client
}

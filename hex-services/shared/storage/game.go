package storage

import (
	"errors"
	"hex-api/hex-services/shared/core"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var (
	ErrGameNotFound = errors.New("game not found in dynamodb")
	gameTableName   = os.Getenv("GAME_TABLE_NAME")
)

// PutGame saves a game in dynamodb
func PutGame(game core.Game) error {
	item, err := dynamodbattribute.MarshalMap(game)
	if err != nil {
		return err
	}

	params := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(gameTableName),
	}
	_, err = dynamoClient.PutItem(params)
	return err
}

// GetGame gets game from dynamodb
func GetGame(id string) (*core.Game, error) {
	key := map[string]*dynamodb.AttributeValue{
		"id": {S: aws.String(id)},
	}

	input := &dynamodb.GetItemInput{
		Key:       key,
		TableName: aws.String(gameTableName),
	}

	output, err := dynamoClient.GetItem(input)
	if err != nil {
		return nil, err
	}

	if len(output.Item) == 0 {
		return nil, ErrGameNotFound
	}

	game := &core.Game{}
	err = dynamodbattribute.UnmarshalMap(output.Item, game)

	return game, err
}

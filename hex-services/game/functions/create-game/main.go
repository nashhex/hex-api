package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"hex-api/hex-services/shared"
	"hex-api/hex-services/shared/apigateway"
	"hex-api/hex-services/shared/core"
	"hex-api/hex-services/shared/storage"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	errUnexpectedInput = errors.New("unexpected input data: missing players ids")
)

func validatePlayers(players []string) error {
	if len(players) < 2 {
		return errUnexpectedInput
	}

	_, err := storage.GetPlayer(players[0])
	if err != nil {
		return err
	}

	_, err = storage.GetPlayer(players[1])
	if err != nil {
		return err
	}

	return nil
}

func apiGatewayHandler(request *events.APIGatewayProxyRequest) (*apigateway.Response, error) {
	game := &core.Game{}

	err := json.Unmarshal([]byte(request.Body), game)
	if err != nil {
		fmt.Println(err)
		return apigateway.NewJSONResponse(500, err), nil
	}

	err = validatePlayers(game.PlayersIDs)
	if err != nil {
		fmt.Println(err)
		return apigateway.NewJSONResponse(400, err), nil
	}

	game.GameID = shared.GenerateID(core.GamePrefix)

	if err := storage.PutGame(*game); err != nil {
		fmt.Println(err)
		return apigateway.NewJSONResponse(500, err), nil
	}

	return apigateway.NewJSONResponse(200, map[string]string{
		"game_id": game.GameID,
	}), nil
}

func main() {
	lambda.Start(apiGatewayHandler)
}

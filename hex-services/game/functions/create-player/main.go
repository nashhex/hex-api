package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"hex-api/hex-services/shared"
	"hex-api/hex-services/shared/apigateway"
	"hex-api/hex-services/shared/core"
	"hex-api/hex-services/shared/storage"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	// ErrMissingStudents error missing students ids
	ErrMissingStudents = errors.New("missing students ids")
)

type request struct {
	*events.APIGatewayProxyRequest
	player *core.Player
}

func (req *request) buildPlayer() error {
	player := &core.Player{}

	err := json.Unmarshal([]byte(req.APIGatewayProxyRequest.Body), player)
	if err != nil {
		return err
	}

	if len(player.StudentsIDs) == 0 {
		return ErrMissingStudents
	}

	player.PlayerID = shared.GenerateID(core.PlayerPrefix)
	req.player = player

	return nil
}

func apiGatewayHandler(req *request) (*apigateway.Response, error) {
	err := req.buildPlayer()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	err = storage.PutPlayer(req.player)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return apigateway.NewJSONResponse(200, map[string]string{
		"player_id": req.player.PlayerID,
	}), nil
}

func main() {
	lambda.Start(apiGatewayHandler)
}
